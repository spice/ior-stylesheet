# XSLT stylesheet for IORs

This stylesheet allows converting Solar Orbiter IOR files to HTML files that can be opened in a web browser and displayed in a human-readable form.

Example usage:

```sh
# with xsltproc (Debian package: xsltproc):
xsltproc IOR.xsl IOR_S_S281F02_SSPI_V1.SOL > IOR.html
# or, with Saxon (Debian package: libsaxonhe-java):
# java -jar /usr/share/java/Saxon-HE.jar IOR_S_S281F02_SSPI_V1.SOL IOR.xsl > IOR.html
firefox ./IOR.html
```

Caveats:

* Not all elements are displayed. In case there is an issue with an element that is not displayed, the issue will not be visible in the output.
* The `soc.solarorbiter.org` namespace name in the IORs is malformed and this can generate a warning during processing. This is an issue with ESA-provided XML schema for the IOR files, but it is not important for the processing.
* The display of IOR comments has been designed specifically to match the use of these comments by the Solar Orbiter/SPICE pipeline.
