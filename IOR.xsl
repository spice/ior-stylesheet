<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xpath-default-namespace="soc.solarorbiter.org" xmlns:in="soc.solarorbiter.org"
exclude-result-prefixes="in">
<xsl:output method="html" version="5" encoding="UTF-8" indent="yes" />
<!--<xsl:output method="xml" encoding="UTF-8" indent="yes" />-->


<!-- Override text built-in -->
<xsl:template match="@*|text()"/>


<xsl:template match="/in:planningData/in:commandRequests">
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"></link>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <title><xsl:value-of select="in:header2/@instrument"/> IOR for <xsl:value-of select="in:header/in:validityRange/in:startTime"/> to <xsl:value-of select="in:header/in:validityRange/stopTime"/></title>
    <style>
        a.anchor-link::after{content:"☐"}
        div:target a.anchor-link::after{content:"☒"}
        div:target{background-color: #ffd}
    </style>
</head>
<body>
    <div class="container">
        <h1><xsl:value-of select="in:header2/@instrument"/> IOR for <xsl:value-of select="in:header/in:validityRange/in:startTime"/> to <xsl:value-of select="in:header/in:validityRange/in:stopTime"/></h1>

        <h2>IOR parameters</h2>

        <ul>
            <li>
                <b>Generation time: </b>
                <xsl:value-of select="in:header/in:genTime"/>
            </li>
            <li>
                <b>VSTP slot count: </b>
                <xsl:value-of select="in:header2/@VSTP_slot_count"/>
            </li>
            <li>
                <b>Planning cycle: </b>
                <xsl:value-of select="in:header2/@planningCycle"/>
            </li>
            <li>
                <b>STP: </b>
                <xsl:value-of select="in:header2/@missionSTP"/>
            </li>
            <li>
                <b>IOR or slot number in STP: </b>
                <xsl:value-of select="in:header2/@IORorSlotNumberInSTP"/>
            </li>
            <li>
                <b>IOR version: </b>
                <xsl:value-of select="in:header2/@IORVersion"/>
            </li>
            <li>
                <b>MIB version: </b>
                <xsl:value-of select="in:header2/@MIBVersion"/>
            </li>
            <li>
                <b>E-FECS version: </b>
                <xsl:value-of select="in:header2/@EFECSVersion"/>
            </li>
            <li>
                <b>TMC version: </b>
                <xsl:value-of select="in:header2/@TMCVersion"/>
            </li>
            <li>
                <b>Sequence count: </b>
                <xsl:value-of select="in:occurrenceList/@count"/>
            </li>
            <li>
                <b>Creation time: </b>
                <xsl:value-of select="in:occurrenceList/@creationTime"/>
            </li>
            <li>
                <b>Author: </b>
                <xsl:value-of select="in:occurrenceList/@author"/>
            </li>
        </ul>

        <h2>
            List of sequences
            <span class="badge badge-pill badge-primary">
                <xsl:value-of select="count(in:occurrenceList/in:sequence)"/>
            </span>
        </h2>

       <xsl:apply-templates select="in:occurrenceList"/>

    </div>
</body>
</html>
</xsl:template>


<xsl:template match="comment()">
    <li>Comment: <xsl:value-of select="."/></li>
</xsl:template>


<xsl:template match="in:occurrenceList">
    <xsl:apply-templates select="in:sequence"/>
</xsl:template>


<xsl:template match="in:sequence">
    <xsl:variable name="sequence_index" select="count(preceding-sibling::in:sequence)"/>
    <div id="{concat('seq-', $sequence_index)}">
        <h3>
            <div class="btn-group mr-2" role="group" aria-label="Previous/Next sequence links">
                <xsl:if test="following-sibling::in:sequence">
                    <a type="button" class="btn btn-outline-primary" href="{concat('#seq-', $sequence_index + 1)}">↓</a>
                </xsl:if>
                <a type="button" class="btn btn-outline-primary anchor-link" href="{concat('#seq-', $sequence_index)}"/>
                <xsl:if test="preceding-sibling::in:sequence">
                    <a type="button" class="btn btn-outline-primary" href="{concat('#seq-', $sequence_index - 1)}">↑</a>
                </xsl:if>
            </div>
            <xsl:value-of select="$sequence_index"/>.
            <xsl:value-of select="in:executionTime/in:actionTime"/>:
            <xsl:value-of select="@name"/>
        </h3>

        <ul>
            <xsl:apply-templates select="comment()"/>
            <li><b>Observation ID: </b> <xsl:value-of select="in:observationID"/></li>
            <li><b>Unique ID: </b> <xsl:value-of select="in:uniqueID"/></li>
        </ul>
        <xsl:if test="count(in:parameterList/in:parameter) &gt; 0">
        <p>Parameters:</p>
        <ul>
            <xsl:apply-templates select="in:parameterList"/>
        </ul>
        </xsl:if>
    </div>
</xsl:template>


<xsl:template match="in:parameterList">
    <xsl:apply-templates select="comment()"/>
    <xsl:apply-templates select="in:parameter"/>
</xsl:template>


<xsl:template match="in:parameter">
    <li>
        <b><xsl:value-of select="@position"/> / <xsl:value-of select="@name"/>: </b>
        <xsl:value-of select="in:value"/> (<xsl:value-of select="in:value/@representation"/>)<xsl:text/>
    </li>
</xsl:template>

</xsl:stylesheet>
